#include "server.h"

uint8_t mess_in[30];
uint8_t mess_out[20];
uint8_t d[1] = {0x23};

uint8_t type;
uint8_t action;
uint8_t device;
uint8_t device_type;
uint8_t data[19];  	

uint8_t code[5]; 
uint8_t status[1];

//intensità led pwm
uint8_t int1 = 0;
uint8_t int2 = 0;
uint8_t int3 = 0;
uint8_t int4 = 0;

ISR(USART3_RX_vect, ISR_BLOCK) {
	
	memset(mess_in, 0, 30);
	UART_getString(mess_in);
	_delay_ms(1000);
	
	//invio ACK di ricezione
  	UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);  
  	UART_putString(mess_out);  	
  	
  	message_translator(mess_in);	
	_delay_ms(1000); 
	
	int i, j; 
	
	switch(type) {
		case 2:
			eeprom_get_code(code);
			for(i = 0; i < 5; i++) {
				if (data[i] != code[i]) {
					memset(mess_out, 0, 20);
					UART_createBuf(mess_out, 6, 0x61, 0x23, 0x23, d, 1);
  					UART_putString(mess_out);
  					break;
				}
			} 
			
			_delay_ms(1000); 
			
			uint8_t house[210];
			eeprom_get_house_struct(house);
			
			UART_putString(house);
			
			break;
			
			//nel client sistemo la struttura e stampo info su azioni
				
		case 3:
			//cambia codice casa 
			if(action == 0x2a) {
				uint8_t new_code[5];
				j = 0;
				for (i = 0;  i < 5; i++) {
					code[i] = data[i];
				}
				for (i = 5;  i < 10; i++) {
					new_code[j] = data[i];
					j++;
				}
				eeprom_change_code(code, new_code);
				UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  				UART_putString(mess_out);			
			}
			//elimina device
			else if(action == 0x5c) {
				eeprom_free_device(device);
				UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  				UART_putString(mess_out);
			}
			//elimina casa
			else if(action == 0x2f) {
				eeprom_get_code(code);
				for(i = 0; i < 5; i++) {
					if (data[i] != code[i]) {
						UART_createBuf(mess_out, 6, 0x61, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
  						break;
					}
				}
				eeprom_free_house();
			}
			break;		
		case 4:
			;
			int ok = 0;
			//accendi
			if(action == 0x2b) {
				ok = eeprom_change_dev_status(device, '1');
				if(ok) {
					if(device == 0x4B) {
						mov_on(); //sensore mov
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					//led pwm 
					else if(device == 0x41 || device == 0x42 || device == 0x43 || device == 0x44) {			
						pwm_change(device, 1);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					} 
					//led normali 
					else {
						led_on(device);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
				}
				break;
			}
			//spegni
			else if(action == 0x2d) {
				ok = eeprom_change_dev_status(device, '0');
				if(ok) {
					if(device == 0x4B) {
						mov_off(); //sensore mov
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
  					}
					//led pwm 
					else if(device == 0x41 || device == 0x42 || device == 0x43 || device == 0x44) {			
						pwm_change(device, 0);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					} 
					//led normali 
					else {
						led_off(device);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
				}
				break;			
			}
			//info
			else if(action == 0x3f) {
				if(device == 0x4C) {
					th_hum_read(device);
				}
				else {
					status[0] = eeprom_get_dev_status(device);
					UART_createBuf(mess_out, 1, 0x23, device, 0x23, status, 1);  
  					UART_putString(mess_out);	
				}
				break;
			}
			//regola + (per pwm)
			else if(action == 0x3e) {
				if(device == 0x41) {
					if(int1 == 5) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int1++;
						pwm_change(device, int1);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
				else if(device == 0x42) {
					if(int2 == 5) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int2++;
						pwm_change(device, int2);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
				else if(device == 0x43) {
					if(int3 == 5) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int3++;
						pwm_change(device, int3);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
				else if(device == 0x44) {
					if(int4 == 5) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int4++;
						pwm_change(device, int4);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
			}
			//regola - (per pwm)
			else if(action == 0x3c) {
				if(device == 0x41) {
					if(int1 == 0) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int1--;
						pwm_change(device, int1);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
				else if(device == 0x42) {
					if(int2 == 0 || int2 == 1) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int2--;
						pwm_change(device, int2);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
				else if(device == 0x43) {
					if(int3 == 0 || int3 == 1) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int3--;
						pwm_change(device, int3);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
				else if(device == 0x44) {
					if(int4 == 0 || int4 == 1) {
						UART_createBuf(mess_out, 6, 0x63, device, 0x23, d, 1);  
  						UART_putString(mess_out);
					}
					else {
						int4--;
						pwm_change(device, int4);
						UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  						UART_putString(mess_out);
					}
					break;				
				}
			}			
		case 5:
			//crea codice casa
			if(action == 0x3F) {
				for(i = 0; i < 5; i++) {
					code[i] = data[i];
				}
				eeprom_init();
				eeprom_add_code(code);	
				UART_createBuf(mess_out, 1, 0x23, 0x23, 0x23, d, 1);
  				UART_putString(mess_out);			
			}
			//crea device
			else if(action == 0x40) {
				eeprom_add_device(device_type, data);
			}
			break;
			
		case 6:
			eeprom_all_off();
			led_all_off();
			pwm_all_off();
			mov_on();   //?
			break;
	}
}


uint8_t num_bit(uint8_t* mess) {
	uint8_t b = 0;
	while(*mess){
    		b += 8;
    		++mess;
  	}
  	return b;
}


//inserisce nelle variabili globali i vari campi del messaggio utili alla gestione delle azioni 
void message_translator(uint8_t* mess) {
	int i, j;
	uint8_t bit = num_bit(mess) - 8;  //meno gli 8 bit di checksum
	//controllo checksum
	if (bit != mess[0]) {
		uint8_t error_mess[5];
		UART_createBuf((uint8_t*)error_mess, 6, 0x65, 0x23, 0x23, d, 1);
  		UART_putString(error_mess);
  		return;	
	}
	
	memset(data, 0, 19);
	type = mess[1];
	switch (type) {
		case 0x1e: 		//ENTRA
			type = 2;
			j = 0;
			for (i = 2; i < 7; i++) {
				data[j] = mess[i];
				j++;			
			}
			break;
		case 0x3d:  		//MODIFICA
			type = 3;
			action = mess[2];
			if (action == 0x2f) {		//elimina casa
				j = 0;
				for (i = 3; i < 8; i++) {
					data[j] = mess[i];
					j++;			
				}
			}
			else if (action == 0x5c) {	//elimina device
				device = mess[3];			
			}
			else if (action == 0x2a){	//cambia codice
				j = 0;
				for (i = 3; i < 13; i++) {
					data[j] = mess[i];
					j++;			
				}
			}
			break;
		case 0x5e: 		//AZIONE
			type = 4;
			action = mess[2];
			device = mess[3];
			break;
		case 0x26: 		//CREA
			type = 5;
			action = mess[2];
			j = 0;
			if (action == 0x3F) {		//codice casa
				for (i = 3; i < 8; i++) {
					data[j] = mess[i];
					j++;			
				}			
			}
			else if (action == 0x40){	//device
				device_type = mess[3];
				i = 4;
				while (mess[i] != 0x0A) { //terminatore \n
					data[j] = mess[i];
					i++;
					j++;	
				}
			}
			break;
		case 0x58:		//ESCI
			type = 6;
			break;
	}
	return;
}





int main(void) {
	//inizializza UART per interrupt 
	//inizializza dispositivi 
	//attedi in loop gli interrupt con messaggi
	
	UART_init();
	led_init();
	pwm_init();
	
	sei();    		//serve per abilitare gli interrupt
		
	while(1) {		
		_delay_ms(1000);
	}
}	
