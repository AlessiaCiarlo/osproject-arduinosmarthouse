#include "client.h"

#define BAUD_RATE 9600


int _socket; 
char dest[18] = "98:D3:61:F5:F6:42";			
char mess_out[50];
char mess_in[50];
char d[1] = {0x23};
char data[10];
char c[1];

char input[] = {'\n', '\n', '\n'};
char code[] = {'\n', '\n', '\n', '\n', '\n', '\n', '\n'};
char code1[] = {'\n', '\n', '\n', '\n', '\n', '\n', '\n'};
char code2[] = {'\n', '\n', '\n', '\n', '\n', '\n', '\n'};
char house_mess[250];
char dev_name[19];
char dev_type[] = {'\n', '\n', '\n'};
house* myhouse;
int size;


int main(int argc, char* argv[]) {
	memset(mess_out, 0, 50);
	memset(mess_in, 0, 50);
	
	_socket = blue_connection(dest);
  	usleep(200000);
  	
	printf("Benvenuto! Io sono Ardi, il tuo gestore di casa intelligente\n e questo è tutto ciò che puoi chiedermi di fare ora:\n 0 - Crea una nuova casa\n 1 - Entra nella mia casa \n Digita il numero corrispondente all'azione desiderata\n Ricorda che posso gestire solo una casa! Se già ne hai una beato te, ma non premere 0 a meno che tu non voglia perderla per sempre!\n\n");
  	
  	printf("$> ");
	fgets(input, 3, stdin);
	while(((input[0]-'0') != 0 && (input[0]-'0') != 1) || (strlen(input) != 2) || (input[1] != '\n')){
		if(input[1] != '\n') free_buf();
		printf("Riprova a digitare il numero corretto, è semplice!\n");
		printf("$> ");
		fgets(input, 3, stdin);
	}
	
	
	//crea nuova casa
	if (input[0]-'0' == 0) {
		
		crea_codice_casa(code1, code2);
		
		//invio al server il nuovo codice casa
		size = strlen(code1);
		bluetooth_createBuf(mess_out, 5, 0x3F, 0x23, 0x23, code1, size);
		blue_send_msg(_socket, mess_out);
		
		printf("\n\nSto creando la tua nuova casa... \n\n"); 
			
		while(ack_rcvd() == 0) {
  			printf("Forse non ti ricevo... riavvia il programma \n\n"); 
  			return 0; 	
  		}
		
		if(error_rcvd()) {
  			printf("Errore nell'invio dei messaggi... riavvia il programma \n\n"); 
  			return 0; 	
  		} 
		
		printf("\nAdesso puoi digitare -1- per entrare nella tua casa\n");
		printf("$> ");
		fgets(input, 2, stdin);
		while(input[0]-'0' != 1) {
			free_buf();
			printf("Entra nella tua casa digitando 1\n");
			printf("$> ");
			fgets(input, 2, stdin);
		}
		
		free_buf();
		in_tha_house();			
	}
	
	//entra nella tua casa
	else if (input[0]-'0' == 1) {
		in_tha_house();
	}	
}



////////////////////////////////////////////////////////////////////////////
void in_tha_house(void) {
	int i, j;
	printf("Eccoci qui! Inserisci il codice di ingresso\n");
	
	entra_in_casa(_socket, mess_out, code);
	
  	printf("\n\nSto entrando nella tua casa... \n\n");
  	
  	while(ack_rcvd() == 0) {
  		printf("Forse non ti ricevo... riavvia il programma \n\n"); 
  		return; 	
  	}
	
	blue_rcv_msg(_socket, house_mess);
	

	while(house_mess[1] == 0x5f) {
		if(house_mess[2] == 0x61) {
			printf("Codice errato! Reinserisci il codice di ingresso\n");
			
			entra_in_casa(_socket, mess_out, code);
			
  			printf("\n\nSto entrando nella tua casa... \n\n");
  			
  			while(ack_rcvd() == 0) {
  				printf("Forse non ti ricevo... riavvia il programma \n\n"); 
  				return; 	
  			}
  			
  			memset(house_mess, 0, 250);
			blue_rcv_msg(_socket, house_mess);
		}
		else if(house_mess[2] == 0x65) {
			printf("Errore nell'invio dei messaggi... riavvia il programma \n\n"); 
  			return;
		}
	}
	
	myhouse = (house*)malloc(sizeof(house));	
	//setto la struttura
	for(i = 0; i < 5; i++) {
		myhouse->house_code[i] = house_mess[i];
	}	
	i = 5;
	j = 0;
	device* dev = (device*)malloc(sizeof(device));
	myhouse->head = dev;
	for(int k = 0; k < 9; k++) {
		set_device_struct(house_mess, dev, i);
		dev->next = (device*)malloc(sizeof(device));
		dev = dev->next;
		i += 20;
	} 
	set_device_struct(house_mess, dev, i);
	dev->next = NULL;	
	
	
	//stampo la struttura
	printf("\n\nQuesta è la struttura della tua casa, con 10 spazi liberi per i tuoi device!\n");
	dev = myhouse->head;
	for(int i = 0; i < 9; i++) {
		print_devices(dev);
		dev = dev->next;
	}
	print_devices(dev); 

	stampa_comandi();
	
	
	while(1) {
		memset(mess_out, 0, 50);
		memset(mess_in, 0, 50);
		
    		printf("$> ");
    		fgets(input, 3, stdin);
    		
    		
    		while((strlen(input) != 2) || (input[1] != '\n')) {
			if(input[2] != '\n') free_buf();
			printf("Numero di cifre errato! Reinserisci il comando\n");
			printf("$> ");
			fgets(input, 3, stdin); 
		}		
		
		switch(input[0]-'0') {
			//lista device 
			case 1: 
				printf("\n");
				//stampo la struttura
				dev = myhouse->head;
				for(int i = 0; i < 9; i++) {
					print_devices(dev);
					dev = dev->next;
				}
				print_devices(dev); 
			
				stampa_comandi();
				break;
							
			//cambia codice casa
			case 2:
			
				cambia_codice_casa(_socket, mess_out, code1, code2);
				
				printf("\nSto cambiando il codice della tua casa... \n \n");
  			
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();	
  					break;
  				}
  				
  				if(error_rcvd()) {
  					printf("Errore di sintassi! Codice precedente scritto in maniera errata \n\n");
  					stampa_comandi();			
  					break;	
  				}
  				
  				printf("\n Codice cambiato correttamente! \n\n");
  			
  				stampa_comandi();
  				break;
  				
  			//crea nuovo device	
			case 3:
			
				crea_device(_socket, mess_out, dev_name, dev_type);
				
				printf("\nSto creando il tuo device... \n \n");
				
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();	
  					break;
  				}
  				
  				if(error_rcvd()) {
  					printf("Errore di spazio! Non puoi più aggiungere device di questo tipo \n\n");
  					stampa_comandi();	
  					break;  				
  				}
  			  				
  				dev = myhouse->head;
  				for(i = 0; i < 10; i++) {
  					if(dev->byte == (char)mess_in[2]) {
  						for(j = 0; j < 17; j++) {
  							dev->dev_name[j] = dev_name[j];
  						}
  					}
  					dev = dev->next;			
  				}
  				printf("\nCreato %s\n\n", dev_name);
  				
				stampa_comandi();
				break;
				
			//elimina device	
			case 4: 
				//stampo la struttura
				dev = myhouse->head;
				for(int i = 0; i < 9; i++) {
					print_devices(dev);
					dev = dev->next;
				}
				print_devices(dev); 
				
				
				elimina_device(_socket, mess_out, dev_type);
				
				printf("\nSto eliminando il device richiesto... \n \n");
				
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();	
  					break;
  				}
  				
  				if(error_rcvd()) {
  					printf("Errore di sintassi! Device richiesto non esistente \n\n");
  					stampa_comandi();
  					break;  				
  				}
				
				
				dev = myhouse->head;
				for(i = 0; i < 10; i++) {
  					if(dev->byte == (char)toupper(dev_type[0])) {
  						for(j = 0; j < 17; j++) {
  							dev->dev_name[j] = ' ';
  						}
  						dev->dev_name[0] = 'f';
  						break;
  					}
  					dev = dev->next;
  				}
  				
  				printf("\nDevice eliminato\n\n");
			
				stampa_comandi();
				break;
				
			//dimmi temperatura
			case 5:
			
				chiedi_temperatura(_socket, mess_out);
			
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();
  					break;
  				}
  				
  				memset(mess_in, 0, 50);
  				blue_rcv_msg(_socket, mess_in);
  				if(mess_in[1] == 0x5f) {
  					printf("Errore di lettura! Impossibile leggere il valore dal device \n\n");
  					stampa_comandi();	
  					break; 
  				}
  				
  				
  				stampa_temperatura(mess_in);
  			
  				stampa_comandi();
				break;
				
			//accendi un device 
			case 6:
				//stampo la struttura
				dev = myhouse->head;
				for(int i = 0; i < 9; i++) {
					print_devices(dev);
					dev = dev->next;
				}
				print_devices(dev); 
				
				accendi_device(_socket, mess_out, dev_type);
				
				printf("\nSto accendendo il device richiesto... \n \n");
				
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();	
  					break;
  				}
  				
  				if(error_rcvd()) {
  					if(mess_in[2] == 0x63) {
  						printf("Device già settato come richiesto \n\n");
  						stampa_comandi();	
  						break; 
  					}
  					else if(mess_in[2] == 0x62) {
  						printf("Errore di sintassi! Device richiesto non esistente \n\n");
  						stampa_comandi();
  						break;	
  					}
  				}
  				
  				printf("\nDevice acceso!! \n \n");
  				
  				stampa_comandi();	
  				break;	
  			
  			//spegni un device
  			case 7:
  				//stampo la struttura
				dev = myhouse->head;
				for(int i = 0; i < 9; i++) {
					print_devices(dev);
					dev = dev->next;
				}
				print_devices(dev); 
				
				
				spegni_device(_socket, mess_out, dev_type);
				
				printf("\nSto spegnendo il device richiesto... \n \n");
  			
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();
  					break;
  				}
  				
  				if(error_rcvd()) {
  					if(mess_in[2] == 0x63) {
  						printf("Device già settato come richiesto \n\n");
  						stampa_comandi();	
  						break; 
  					}
  					else if(mess_in[2] == 0x62) {
  						printf("Errore di sintassi! Device richiesto non esistente \n\n");
  						stampa_comandi();	
  						break;	
  					}
  				}
  				
  				printf("\nDevice spento!! \n \n");
  				
  				stampa_comandi();
  				break;				
  				
  				
  			//Regola l'intensità di una luce
  			case 8:
  			
  				regola_luce(_socket, mess_out, dev_type, input, dev, myhouse);
  				
  				printf("\nSto regolando il device richiesto... \n \n");
  				
				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();
  					break;
  				}
  				
  				if(error_rcvd()) {
  					printf("Device già settato come richiesto \n\n");
  					stampa_comandi();
  					break; 
  				}
  				
  				printf("\nDevice regolato!! \n \n");
  				
  				stampa_comandi();
  				break;			
			
			//info su un device
			case 9:	
				//stampo la struttura
				dev = myhouse->head;
				for(int i = 0; i < 9; i++) {
					print_devices(dev);
					dev = dev->next;
				}
				print_devices(dev); 
				
				printf("\nSto chiedendo info sul device richiesto... \n \n");				
				
				info_device(_socket, mess_out, dev_type);
				
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();	
  					break;
  				}
  				
  				if(error_rcvd()) {
  					printf("Errore di sintassi! Device richiesto non esistente \n\n");
  					stampa_comandi();
  					break;	
  				}
				
  				if(mess_in[3] == '1') printf("\nDevice acceso!  \n \n");
  				else if(mess_in[3] == '0') printf("\nDevice spento!  \n \n");
  				
  				stampa_comandi();
  				break;				
  			
  			//esci
  			case 0:
  				size = strlen(d);
  				bluetooth_createBuf(mess_out, 6, 0x23, 0x23, 0x23, d, size);
  				blue_send_msg(_socket, mess_out);
  				
  				if(ack_rcvd() == 0) {
  					printf("Forse non ti ricevo... reinserisci il comando \n\n");  
  					stampa_comandi();	
  					break;
  				}
  				
  				blue_close(_socket);
  				free_struct(myhouse);
  				printf("Stai per uscire... ci vediamo alla prossima!\n");
  				return; 
  			
  			default: 
  			 	printf("Inserisci il numero giusto!\n");
  			 	break;				
  		}
  	}
}


////////////////////////////////////////////////////////////////////////
int ack_rcvd(void) {
	memset(mess_in, 0, 50);
	blue_rcv_msg(_socket, mess_in);
	//printf("%s\n", mess_in);
	if(mess_in[1] == '!') {
		memset(mess_out, 0, 50);
		return 1;
	}
	
	for(int i = 0; i < 3; i++) {
		memset(mess_in, 0, 50);
		blue_send_msg(_socket, mess_out);
		blue_rcv_msg(_socket, mess_in);
		if(mess_in[1] == 0x21) return 1;
	}
	memset(mess_out, 0, 50);
	return 0;
}
////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////
int error_rcvd(void) {
	memset(mess_in, 0, 50);
	blue_rcv_msg(_socket, mess_in);
	if(mess_in[0] == 0) return 0;
	else if (mess_in[1] == 0x5f) return 1;
	else return 0;
}
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
void free_buf(void) {
	while(getchar() != '\n');
}
////////////////////////////////////////////////////////////////////////
