#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>



//house code di 5 cifre (5 byte) 
//ogni volta che viene creata una connessione tra client e server, quest'ultimo invia al client la configurazione della casa salvata sulla eeprom. Il client si creerà una struttura temporanea della casa per poter inviare messaggi coerenti al server.

typedef struct house {
	char house_code[5];
	struct device* head;
}house;

typedef struct device {
	char byte;
	char dev_name[17];
	struct device* next;
}device;


void print_devices(device* dev) {
	printf("%c --> ", dev->byte);
	if((dev->dev_name[0] == 'f') && ((dev->dev_name[1] == '0') || (dev->dev_name[1] == ' '))){
		printf("spazio libero!\n");
		return;	
	}
	for(int j = 0; j < 17; j++) {
		if(j == 16) {
			printf("%c \n", dev->dev_name[16]);
			return;
		}
		else if((dev->dev_name[j] == '0') || (dev->dev_name[j] == '\n')){///////
			printf("\n");
			return;
		}
		printf("%c", dev->dev_name[j]);
	}
	//printf("%c \n", dev->dev_name[16]);
}
/*	printf("%c --> ", dev->byte);
	for(int j = 0; j < 16; j++) {
		printf("%c", dev->dev_name[j]);
	}
	printf("%c \n", dev->dev_name[16]);
*/

void set_device_struct(char* house_mess, device* dev, int d) {
	dev->byte = house_mess[d];
	int j = 0;
	for(int i = d+3; i < d+20; i++) {
		dev->dev_name[j] = house_mess[i];
		j++;
	}
}



void free_struct(house* house) {
	device* d;
	while(house->head != NULL) {
		d = house->head;
		house->head = house->head->next;
		free(d);
	}
	free(house);
}


