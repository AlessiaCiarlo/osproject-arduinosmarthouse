#include "bluetooth.h"


int blue_connection (char* dest){
  	struct sockaddr_rc addr = {0};
  	int s, ret;

  	// alloco socket
  	s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);	

  	// parametri della connessione 
  	addr.rc_family = AF_BLUETOOTH;
  	addr.rc_channel = (uint8_t) 1;
  	str2ba(dest, &addr.rc_bdaddr);

  	// connetto al server
  	ret = connect(s, (struct sockaddr*)&addr, sizeof(addr));
  	if(ret < 0) perror("Errore nella connessione\n\n");
  	else printf("Connessione stabilita\n\n");

  	return s;
}
///////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////
int blue_send_msg(int socket, char* mess){
  	int ret;
  	int mess_len = strlen(mess);		//lunghezza
  	int written = 0;
  	while(written < mess_len){
    		written += write(socket, mess, mess_len - written); 
    		if (written == -1){
     			printf("Errore durante l'invio del messaggio\n");
      			exit(EXIT_FAILURE);
    		}
  	}
  	return written;
}
///////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////
void blue_rcv_msg(int socket, char* mess){
  	int ret;
  	char m[1];
 	int i = 0, j = 0;
  	do {
    		ret = read(socket, m, 1); //leggo un char alla volta
   
    		if (ret == -1){
      			printf("Errore durante la lettura del messaggio\n");
      			return;
    		}
    		else if (ret == 0 && j == 1){
    			return;
    		}
    		else if (ret == 0){
      			usleep(1000); //do più tempo per scrivere (attendo)
      			j++;
      			continue;
    		}
    		mess[i] = m[0];
    		i++;
  	} while(m[0] != '\n');
  	mess[i] = 0; 			//termino la stringa
 
  	return;
}
///////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////
void blue_close(int socket){
  	close(socket);
  	return;
}
///////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////
//creo il messaggio da inviare 
void bluetooth_createBuf(char* buff, int type, uint8_t action, uint8_t device, uint8_t device_type, char* data, int size) {
	
	if (type == 2) { 				// ENTRA 
		buff[0] = 56;
		buff[1] = 0x1e; 			//^^ ENTRA
		buff[2] = data[0];
		buff[3] = data[1];
		buff[4] = data[2];
		buff[5] = data[3];
		buff[6] = data[4];
		buff[7] = '\n'; 			//line feed = \n terminatore
	}
	else if (type == 3) { 				// MODIFICA 
		if (action == 0x2a) { 			//azione = cambia codice 
			buff[0] = 104;
			buff[1] = 0x3d; 		//= MODIFICA
			buff[2] = action; 		//cambia codice
			buff[3] = data[0]; 		//vecchio codice
			buff[4] = data[1];
			buff[5] = data[2];
			buff[6] = data[3];
			buff[7] = data[4]; 
			
			buff[8] = data[5]; 		//nuovo codice
			buff[9] = data[6];
			buff[10] = data[7];
			buff[11] = data[8];
			buff[12] = data[9];
			buff[13] = '\n'; 		//line feed = \n terminatore
		}
		else if(action == 0x5c) {		 		 
			buff[0] = 32;
			buff[1] = 0x3d; 		//= MODIFICA
			buff[2] = action;		//azione = elimina device
			buff[3] = device;
			buff[4] = '\n'; 		//line feed = \n terminatore
		}
		else if(action == 0x2f) {
			buff[0] = 64;
			buff[1] = 0x3d; 		//= MODIFICA
			buff[2] = action;		//azione = elimina casa
			buff[3] = data[0]; 		//codice casa
			buff[4] = data[1];
			buff[5] = data[2];
			buff[6] = data[3];
			buff[7] = data[4]; 
			buff[8] = '\n'; 		//line feed = \n terminatore
		}
	}
	else if (type == 4) { 				// AZIONE 
		buff[0] = 32;
		buff[1] = 0x5e; 			//^ AZIONE
		buff[2] = action;
		buff[3] = device;
		buff[4] = '\n'; 			//line feed = \n terminatore
	}
	else if (type == 5) { 				//CREA
		if (action == 0x3F) { 			//codice casa 
			buff[0] = 64;
			buff[1] = 0x26; 		//& CREA
			buff[2] = action; 		//crea codice casa (prima volta)
			buff[3] = data[0];
			buff[4] = data[1];
			buff[5] = data[2];
			buff[6] = data[3];
			buff[7] = data[4];
			buff[8] = '\n'; 		//line feed = \n terminatore
		}
		else if (action == 0x40) { 		//crea device 
			buff[0] = 32 + (size * 8);    
			buff[1] = 0x26; 		//& CREA
			buff[2] = action; 		//crea device
			buff[3] = device_type; 		//tipo nuovo device 
			for (int i = 0; i < size; i++) { //scrivo il nome del dev nel messaggio
				buff[4 + i] = data[i];
			}
			buff[5 + size - 1] = '\n'; 	//line feed = \n terminatore
		}
	}
	else if (type == 6) { 
		buff[0] = 16;
		buff[1] = 0x58; 			//X ESCI
		buff[2] = '\n';				//line feed = \n terminatore
	}
	

	//printf("%s\n", buff);
	return;
}
///////////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////
int num_bit(char* mess) {
	int b = 0;
	while(*mess){
    		b += 8;
    		++mess;
  	}
  	return b;
}
///////////////////////////////////////////////////////////////////////


