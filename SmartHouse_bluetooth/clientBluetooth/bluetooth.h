#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <string.h>



int blue_connection (char* dest);

int blue_send_msg(int socket, char* mess);

void blue_rcv_msg(int socket, char* mess);

void blue_close(int socket);

void bluetooth_createBuf(char* buff, int type, uint8_t action, uint8_t device, uint8_t device_type, char* data, int size);

int num_bit(char* mess);
