#include <string.h>
#include "UART.h"
#include <avr/eeprom.h>

void eeprom_init (void);

void eeprom_get_house_struct (uint8_t* house);

void eeprom_free_house (void);

void eeprom_add_code (uint8_t* house_code);

void eeprom_free_device (uint8_t dev_byte);

void eeprom_change_code (uint8_t* old_house_code, uint8_t* new_house_code); 

void eeprom_add_device (uint8_t type, uint8_t* dev_name);

int eeprom_change_dev_status (uint8_t dev_byte, uint8_t new_status); 

void eeprom_all_off(void);

uint8_t eeprom_get_dev_status (uint8_t dev_byte); 

int eeprom_get_dev_pin (uint8_t dev_byte);

void eeprom_get_code (uint8_t* code);

int num_byte(uint8_t* mess);
