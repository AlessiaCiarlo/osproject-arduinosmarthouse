#include "dev.h"


void led_init(void) {
	//pin 22,23,24,25,26 --> bit 0,1,2,3,4 della porta A
	const uint8_t mask = 0b00011111; 
    	DDRA |= mask;  			//pin configurati come output
    	PORTA = 0;     			//pin off
}



void led_on(uint8_t led) {
    	uint8_t mask;

	switch (led) {
		case 0x45: 	//led 22
			mask = (1<<0); //0b00000001
			PORTA |= mask;
			break;
		case 0x46:	//led 23
			mask = (1<<1); //0b00000010
			PORTA |= mask;
			break;
		case 0x47:	//led 24
			mask = (1<<2); //0b00000100
			PORTA |= mask;
			break;
		case 0x48:	//led 25
			mask = (1<<3); //0b00001000
			PORTA |= mask;
			break;
		case 0x49:	//led 26
			mask = (1<<4); //0b00010000
			PORTA |= mask;
			break;
	}
	return;
}



void led_off(uint8_t led) {
    	uint8_t mask;

	switch (led) {
		case 0x45:	//led 22
			mask = (1<<0);
			PORTA &= ~(mask);
			break;
		case 0x46:	//led 23
			mask = (1<<1);
			PORTA &= ~(mask);
			break;
		case 0x47:	//led 24
			mask = (1<<2);
			PORTA &= ~(mask);
			break;
		case 0x48:	//led 25
			mask = (1<<3);
			PORTA &= ~(mask);
			break;
		case 0x49:	//led 26
			mask = (1<<4); 
			PORTA &= ~(mask);
			break;
	}
	return;
}



void led_all_off(void) {
	uint8_t mask;

	//led 22
	mask = (1<<0);
	PORTA &= ~(mask);
	
	//led 23
	mask = (1<<1);
	PORTA &= ~(mask);
	
	//led 24
	mask = (1<<2);
	PORTA &= ~(mask);
	
	//led 25
	mask = (1<<3);
	PORTA &= ~(mask);
			
	//led 26
	mask = (1<<4); 
	PORTA &= ~(mask);
}

