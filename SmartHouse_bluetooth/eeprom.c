#include "eeprom.h"


/* 
Scriverò nella eeprom, a partire dal primo byte disponibile:
- il codice di accesso alla casa (tipo password) che occuperà i primi 5 byte [0,4]
- un massimo di 10 device a seguire, per quali sono disponibili 20 byte ciascuno 
	così suddivisi:
		-- primo byte è l'identificativo unico del device (assegnato in base alla 	
		tipologia di dispositivo nel momento della sua creazione)
		-- secondo byte è il pin arduino sul quale è mappato il device
		-- terzo byte indica lo STATUS (on/1, off/0) del device
		-- restanti 17 byte utilizzabili per il nome dato dal client 
		
////////////////////////////////////////////////////////////////////////////////////////////////		
I primi 4 dispositivi salvati sono di tipo LUCI REGOLABILI 
Altri 6 sono ALTRO (luci normali o altri device simulati con led)
1 è il sensore di movimento 
1 è il sensore di temperatura e umidità
///////////////////////////////////////////////////////////////////////////////////////////////

Con la stessa modalità avrei potuto gestire molte più di 10 case differenti (o case con molti più devide connessi) avendo a disposizione 4KB di eeprom e utilizzandone soltanto 200 Byte per casa

Al primo utilizzo del server, questo andrà ad inizializzare la eeprom con i byte identificativi dei device (non ancora assegnati) e con i rispettivi pin su arduino.
*/


void eeprom_init(void) {

	eeprom_free_house(); //metto tutto a 0 fino alla posizione 210

	//tipo luce regolabile ///////////////////////////
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)5, 0x41); 		//lettera A 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)6, 0x38);		//pin su cui è mappato il device (da castare in int)
	eeprom_busy_wait();			
	eeprom_write_byte((uint8_t*)7, '0'); 		//metto a 0 il byte dello status (tutti dispositivi spenti)
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)8, 'f');
	
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)25, 0x42); 		//lettera B 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)26, 0x0B);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)27, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)28, 'f');
	
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)45, 0x43); 		//lettera C
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)46, 0x0C);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)47, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)48, 'f');
	
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)65, 0x44); 		//lettera D 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)66, 0x0D);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)67, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)68, 'f');
	//////////////////////////////////////////////////
	
	//tipo luce normale o altro //////////////////////
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)85, 0x45); 			//lettera E 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)86, 0x16);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)87, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)88, 'f');
	
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)105, 0x46); 		//lettera F 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)106, 0x17);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)107, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)108, 'f');
	
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)125, 0x47); 		//lettera G 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)126, 0x18);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)127, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)128, 'f');
	
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)145, 0x48); 		//lettera H 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)146, 0x19);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)147, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)148, 'f');
	/////////////////////////////////////////////////
	
	
	// sensore di movimento 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)165, 0x4B); 		//lettera K 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)166, 0x15);  	//sensore sul pin 21 poichè abilitato alla ricezione interrupt 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)167, '0');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)168, 'f');
	
	// sensore di temperatura 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)185, 0x4C); 		//lettera L 
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)186, 0x34);
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)187, '1');
	eeprom_busy_wait();
	eeprom_write_byte((uint8_t*)188, 'f');
}



/////////////////////////////////////////////////////////////////////
void eeprom_get_house_struct (uint8_t* house) {
	for (int i = 0 ; i < 210 ; i++) {  	//da 5 iniziano i device
		eeprom_busy_wait();
    		house[i] = eeprom_read_byte((uint8_t*)i);
  	}
  	house[210] = '\n';
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_free_house (void) {
	for (int i = 0 ; i < 210 ; i++) {
		eeprom_busy_wait();
    		eeprom_write_byte((uint8_t*)i, '0');
  	}
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_add_code (uint8_t* house_code) {
	for (int i = 0 ; i < 5 ; i++) {
		eeprom_busy_wait();
    		eeprom_write_byte((uint8_t*)i, house_code[i]);
  	}
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_free_device (uint8_t dev_byte) {
	int j = 5; 				//primo byte in cui si trova il byte riconoscitivo del primo device
	for (int i = 0 ; i < 10 ; i++) {   	//10 numero max di devices
		eeprom_busy_wait();
    		if (eeprom_read_byte((uint8_t*)j) == dev_byte) {
    			for (int k = j+3; k < j+20; k++) {  
    			//metto a zero la parte contenente il nome del device 
    				eeprom_busy_wait();
    				eeprom_write_byte((uint8_t*)k, '0');
    			}
    			//scrivo "f" al posto del nome per ricordarmi che il byte che era 				
    			//associato a quel device può essere riassegnato
    			eeprom_busy_wait();
    			eeprom_write_byte((uint8_t*)j+3, 'f');  
    			return;
    		}
    		j += 20;
  	}
  	//invio messaggio di errore al client di tipo "device non esistente"
  	uint8_t data[1] = {0x23};
  	uint8_t error_mess[5]; 
  	UART_createBuf(error_mess, 6, 0x62, 0x23, 0x23, data, 1);
  	UART_putString(error_mess);
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_change_code (uint8_t* old_house_code, uint8_t* new_house_code) {
	int i;
	uint8_t old[5]; 
	eeprom_get_code(old);
	for(i = 0; i < 5; i++) {
		if (old_house_code[i] != old[i]) {
			uint8_t data[1] = {0x23};
			uint8_t error_mess[5]; 
			UART_createBuf(error_mess, 6, 0x61, 0x23, 0x23, data, 1);
  			UART_putString(error_mess);
  			break;
		}
	}
	
	for (i = 0 ; i < 5 ; i++) {
		eeprom_busy_wait();
    		eeprom_write_byte((uint8_t*)i, new_house_code[i]);
  	}
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_add_device (uint8_t dev_type, uint8_t* dev_name) { 
	uint8_t data[1] = {0x23};	
	uint8_t error_mess[5]; 		//serve nei messaggi 
	uint8_t mess[10]; 
	int i = 0;
	int j = 0;
	int k = 0;
	
	
	//int type = dev_type;
	if (dev_type == '1') {  		//tipo 1 = LUCI REGOLABILI
		j = 8; 	
		while ((eeprom_read_byte((uint8_t*)j) != 'f') && (eeprom_read_byte((uint8_t*)(j+1)) != '0')) {
			eeprom_busy_wait();
			//se non trova nessun "f" di free fino alla fine del tipo 1
			//allora ritorna un codice di errore di tipo 
			//"non più spazio disponibile" che il server invia al client 
			if (j == 68) {
				UART_createBuf(error_mess, 6, 0x64, 0x23, 0x23, data, 1);
  				UART_putString(error_mess);
  				return; 
			}
			j += 20;
		}
	}
	else if (dev_type == '2') {  		//tipo 2 = LUCI e ALTRO
		j = 88;
		while ((eeprom_read_byte((uint8_t*)j) != 'f') && (eeprom_read_byte((uint8_t*)(j+1)) != '0')) {
			eeprom_busy_wait();
			//se non trova nessun "f" di free fino alla fine del tipo 2
			//allora ritorna un codice di errore di tipo 
			//"non più spazio disponibile" che il server invia al client 
			if (j == 148) {
				UART_createBuf(error_mess, 6, 0x64, 0x23, 0x23, data, 1);
  				UART_putString(error_mess);
  				return; 
			}
			j += 20;
		}
	}
	else if (dev_type == '3') {  		//tipo 3 = sensore movimento 
		j = 168;
		eeprom_busy_wait();
		if ((eeprom_read_byte((uint8_t*)j) != 'f') && (eeprom_read_byte((uint8_t*)(j+1)) != '0')) {
			UART_createBuf(error_mess, 6, 0x64, 0x23, 0x23, data, 1);
  			UART_putString(error_mess);
  			return; 
		}
	}
	else if (dev_type == '4') {  		//tipo 3 = sensore temperatura 
		j = 188;	
		eeprom_busy_wait();
		if ((eeprom_read_byte((uint8_t*)j) != 'f') && (eeprom_read_byte((uint8_t*)(j+1)) != '0')) {
			UART_createBuf(error_mess, 6, 0x64, 0x23, 0x23, data, 1);
  			UART_putString(error_mess);
  			return; 
		}
	}
		
	//a questo punto la posizione per inserire il nuovo device è j 
	int byte = num_byte(dev_name);
	for (i = j; i < j + byte; i++) {	//sizeof(dev_name)/sizeof(dev_name[0])
		
		eeprom_busy_wait();
		eeprom_write_byte((uint8_t*)i, dev_name[k]);
		k++;
	}
	eeprom_busy_wait();
	
	uint8_t dev_byte = eeprom_read_byte((uint8_t*)(j-3));
	
	// invia ACK al client con il byte identificativo del nuovo device 
	UART_createBuf(mess, 1, 0x23, dev_byte, 0x23, data, 1);
  	UART_putString(mess);
  	
	return;
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
int eeprom_change_dev_status (uint8_t dev_byte, uint8_t new_status) {
	int j = 5; 				//primo byte in cui si trova il byte riconoscitivo del primo device
	uint8_t data[1] = {0x23};
	uint8_t error_mess[5]; 
	for (int i = 0 ; i < 10 ; i++) {   	//10 numero max di devices
		eeprom_busy_wait();
    		if (eeprom_read_byte((uint8_t*)j) == dev_byte) {
    			eeprom_busy_wait();
    			if (eeprom_read_byte((uint8_t*)j+2) == new_status) {
    				//invio messaggio di errore al client di tipo "device già settato come richiesto"
				UART_createBuf(error_mess, 6, 0x63, 0x23, 0x23, data, 1);
  				UART_putString(error_mess);
  				return 0;
			}
			eeprom_busy_wait();
    			eeprom_write_byte((uint8_t*)j+2, new_status);
    			return 1;
    		}
    		j += 20; 
    	}
    	//invio messaggio di errore al client di tipo "device non esistente"
    	UART_createBuf(error_mess, 6, 0x62, 0x23, 0x23, data, 1);
  	UART_putString(error_mess);
  	return 0;
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_all_off(void) { 			//spegne tutti i device nella eeprom
	int j = 7;
	for (int i = 0 ; i < 10 ; i++) {   	//10 numero max di devices
		eeprom_busy_wait();
		eeprom_write_byte((uint8_t*)j, '0');
		j += 20;
	}
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
uint8_t eeprom_get_dev_status (uint8_t dev_byte) {
	uint8_t status;
	int j = 5; 				//primo byte in cui si trova il byte riconoscitivo del primo device
	for (int i = 0 ; i < 10 ; i++) {   	//10 numero max di devices
		eeprom_busy_wait();
    		if (eeprom_read_byte((uint8_t*)j) == dev_byte) {
    			eeprom_busy_wait();
    			status = eeprom_read_byte((uint8_t*)j+2);
    			return status;
    		}
    		j += 20;
    	}
    	uint8_t data[1] = {0x23};
    	uint8_t error_mess[5]; 
    	//invio messaggio di errore al client di tipo "device non esistente"
    	UART_createBuf(error_mess, 6, 0x62, 0x23, 0x23, data, 1);
  	UART_putString(error_mess);
 	return 0x23;
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
int eeprom_get_dev_pin (uint8_t dev_byte) {
	int pin;
	int j = 5; 				//primo byte in cui si trova il byte riconoscitivo del primo device
	for (int i = 0 ; i < 10 ; i++) {   	//12 numero max di devices
		eeprom_busy_wait();
    		if (eeprom_read_byte((uint8_t*)j) == dev_byte) {
    			eeprom_busy_wait();
    			pin = (int)eeprom_read_byte((uint8_t*)j+1);
    			return pin;
    		}
    		j += 20;
    	}
 	return 0;
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void eeprom_get_code (uint8_t* code) {
	for (int i = 0 ; i < 5 ; i++) {
		eeprom_busy_wait();
    		code[i] = eeprom_read_byte((uint8_t*)i);
  	}
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
int num_byte(uint8_t* mess) {
	int b = 0;
	while(*mess){
    		b += 1;
    		++mess;
  	}
  	return b;
}
/////////////////////////////////////////////////////////////////////








