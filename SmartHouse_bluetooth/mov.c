#include "dev.h"

//sensore di movimento con interrupt esterno --> pin 21 

int mov;

ISR(INT0_vect) {
  	movalarm();
}

void movalarm(void) {
	led_on(0x49);
	_delay_ms(1000);
	led_off(0x49);
}

void mov_on(void) {
	_delay_ms(1000);
	
	DDRD &= ~(1<<PD0); 		//input
	PORTD |= (1<<PD0); 		//resistor pull up		

	EIMSK |= 1<<INT0;		//abilito l'interrupt 0
	EICRA = 1<<ISC00|1<<ISC01;	//trigger sul fronte di salita
	 
	_delay_ms(1000);
	
	sei();
}

void mov_off(void) {

	EIMSK &= ~(1<<INT0);		//disabilito l'interrupt
	EICRA = 0<<ISC00|0<<ISC01;
}


