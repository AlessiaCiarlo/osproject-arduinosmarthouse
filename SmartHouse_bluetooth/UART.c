#include "UART.h"

#define BAUD 9600
#define MYUBRR (F_CPU/16/BAUD-1)

/////////////////////////////////////////////////////////////////////
void UART_init(void){
  	// Set baud rate
  	UBRR3H = (uint8_t)(MYUBRR>>8);
  	UBRR3L = (uint8_t)MYUBRR;

  	UCSR3C = (1<<UCSZ31) | (1<<UCSZ30); /* 8-bit data */ 
  	UCSR3B = (1<<RXEN3) | (1<<TXEN3) | (1<<RXCIE3);   /* Enable RX and TX */  
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void UART_putChar(uint8_t c){
  	// wait for transmission completed, looping on status bit
  	while ( !(UCSR3A & (1<<UDRE3)) );

  	// Start transmission
  	UDR3 = c;
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
uint8_t UART_getChar(void){
  	// Wait for incoming data, looping on status bit
  	while ( !(UCSR3A & (1<<RXC3)) );
  
  	// Return the data
  	return UDR3;
    
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
// reads a string until the first newline or 0
// returns the size read
uint8_t UART_getString(uint8_t* buf){
  	uint8_t* b0 = buf; //beginning of buffer
  	uint8_t c;
  	while(1){
    		c = UART_getChar();
    		*buf = c;
    		++buf;
    		// reading a 0x0A (\n) terminates the string
    		if (c == 0x0A)
      			return buf-b0;
  	}
}
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
void UART_putString(uint8_t* buf){
  	while(*buf != '\n'){
    		UART_putChar(*buf);
    		++buf;
  	}
  	UART_putChar(*buf);
}
/////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////
//creo il messaggio da inviare usando poi UART_put e UART_get
void UART_createBuf(uint8_t* buff, int type, uint8_t action, uint8_t device, uint8_t device_type, uint8_t* data, int size) {
	
	//memset(buff, '\n', 20);
	if (type == 1) { 				// ACK 
		buff[0] = 24 + (size * 8);  		//checksum --> numero di bit inviati
		buff[1] = 0x21; 			//! ACK
		buff[2] = device; 			//se si tratta di dispositivo creato (altrimenti 0x23)
		for (int i = 0; i < size; i++) {  	//se si tratta di ACK con info (temperatura e mov)
			buff[3 + i] = data[i];
		} 
		buff[4+size-1] = '\n'; 			//line feed = \n terminatore
	}
	else if (type == 6) { 				//ERRORE
		buff[0] = 24;
		buff[1] = 0x5f; 			//_ ERRORE
		buff[2] = action;  			// tipo di errore 
		buff[3] = '\n'; 			//line feed = \n terminatore
	}
	
	return;
}
/////////////////////////////////////////////////////////////////////

