# OSProject- ArduinoSmartHouse

**CLIENT**
-------------------------------------------------------------------------------
ISTRUZIONI PER L'ESECUZIONE DEL CLIENT
- aprire un terminale ed andare nella cartella "client" qui contenuta
- digitare $>make e premere invio
- digitare ./client e premere invio
- seguire le istruzioni date man mano
-------------------------------------------------------------------------------

Il client, una volta connesso con il server tramite file aperto in lettura e scrittura o socket connesso al bluetooth, permette all’utente di scegliere se entrare nella sua casa o crearne una nuova. Dopo di che gli verrà inviato un elenco di devices con i rispettivi nomi (se già creati) e identificativi (lettere dalla A alla L – 10 devices). 
Il client si occuperà di popolare una struttura di tipo Linked list contenente tutti questi devices per poterli ricordare all’utente quando necessario.
Il client a questo punto può inviare richieste al server attraverso messaggi implementati come array di char con, in prima posizione, il checksum. Questi possono essere di 5 tipologie:

- ENTRA --> per entrare in casa, previo inserimento del codice di accesso
- MODIFICA --> per modificare il codice della casa 
- AZIONE --> accendere, spegnere, avere info, regolare pwm+, regolare pwm- di un device
- CREA --> creare nuovo codice casa oppure un nuovo device
- ESCI --> per comunicare al server che si sta uscendo dalla casa

Ogni messaggio sarà composto in modo diverso, a seconda della tipologia, dalla funzione UART_createBuf presente sia nel client che nel server.
L’utente avrà a disposizione una serie da azioni tra le quali scegliere digitando il rispettivo numero.



**SERVER**

Il server attenderà l’arrivo di richieste da parte del client rimanendo a riposo e venendo svegliato tramite interrupt sull’UART o su pin RX-TX conessi al modulo bluetooth. Successivamente si preoccuperà di inviare immediatamente un messaggio di tipo ACK di ricezione al client (che lo coglierà e ne sarà felice) e di passare il messaggio ricevuto ad una funzione apposita incaricata di spezzettarlo. Questa farà prima un controllo sul checksum e poi, a seconda della tipologia di messaggio, incapsulerà i dati significativi in variabili globali utili per lo svolgimento delle varie attività. Verrà, quindi, evasa la richiesta del client permettendo al server di rimettersi a riposo.

A seconda del tipo di richiesta il server;
- invierà messaggi al client tramite UART (messaggi di tipo ack, info o errore)  
- modificherà lo stato dei device richiesti
- scriverà/leggerà informazioni da EEPROM 



_**Tutti i tipi di messaggi**_

1 --> ACK (0x21)  =  checksum + 0x21 + terminatore

2 --> ENTRA (0x1e) =  checksum + 0x1e + codice casa + terminatore

3 --> MODIFICA (0x3d) =  checksum + 0x3d + vecchio codice casa U nuovo codice casa + terminatore

4 --> AZIONE (0x5e) =  checksum + 0x5e + _ACTIONS DEVICE_ (vedi dopo) + device + terminatore

5 --> CREA (0x26) =  checksum + 0x26 + _ACTIONS CASA_ (vedi dopo) + dati + terminatore

6 --> ERRORE (0x5f) ) =  checksum + 0x5f + _TIPO ERRORE_ (vedi dopo) + terminatore

7 --> ESCI (0x58) =  checksum + 0x58 + terminatore



**_ACTIONS DEVICE_** = accendi (0x2b), spegni (0x2d), info (0x3f), regola+ (0x3e), regola- (0x3c)

**_ACTIONS CASA_** = crea codice (0x3F), crea device (0x40)

**_TIPO ERRORE_** = errore di sintassi (0x61), device non esistente (0x62), device già settato come richiesto (0x63), non più spazio disponibile per un device (0x64), messaggio non arrivato totalmente (0x65)
