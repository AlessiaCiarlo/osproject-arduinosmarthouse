#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void UART_init(void);

void UART_putChar(uint8_t c);

uint8_t UART_getChar(void);

uint8_t UART_getString(uint8_t* buf);

void UART_putString(uint8_t* buf);

void UART_createBuf(uint8_t* buff, int type, uint8_t action, uint8_t device, uint8_t device_type, uint8_t* data, int size);
