#include "dev.h"

//sensore di movimento con interrupt esterno --> pin 21 

int mov;

//Interrupt Service Routine che parte quando si ha una variazione 
//sul fronte di salita sul pin al quale è connesso 
ISR(INT0_vect) {
  	movalarm();
}

void movalarm(void) {
	led_on(0x49);
	_delay_ms(1000);
	led_off(0x49);
}

//inizializza il pin del sensore
void mov_on(void) {
	_delay_ms(1000);
	
	DDRD &= ~(1<<PD0); 		//come input
	PORTD |= (1<<PD0); 		//resistor pull up		

	EIMSK |= 1<<INT0;		//abilita l'interrupt 0
	EICRA = 1<<ISC00|1<<ISC01;	//trigger sul fronte di salita
	 
	_delay_ms(1000);
	sei();				//si mette in attesa di interrupt
}

void mov_off(void) {
	EIMSK &= ~(1<<INT0);		//disabilita l'interrupt
	EICRA = 0<<ISC00|0<<ISC01;
}


