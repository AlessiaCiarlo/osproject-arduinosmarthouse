#include "serial_fd.h"
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "house.h"

#define BAUD_RATE 9600

void in_tha_house(void);

int ack_rcvd(void);

int error_rcvd(void);

void free_buf(void);

/////////////////////////////////////////////////////////////////////////////////////
void stampa_comandi(void) {
	printf("\nTi ricordo i comandi che puoi darmi:\n 1 - Lista device\n 2 - Cambia codice casa\n 3 - Aggiungi un device\n 4 - Elimina un device\n 5 - Dimmi la temperatura\n 6 - Accendi un device\n 7 - Spegni un device\n 8 - Regola l'intensità di una luce\n 9 - Dammi info su un device\n 0 - Esci\n\n");
	return;
}
/////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////
void entra_in_casa(int fd, char* mess_out, char* code) {
	
	printf("$> ");
	fgets(code, 7, stdin);
	while((strlen(code) != 6) || (code[5] != '\n')) {
		if(code[6] != '\n') free_buf();
		printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
		printf("$> ");
		fgets(code, 7, stdin); 
	}
	int size = strlen(code);
	
	memset(mess_out, 0, 50);
	UART_createBuf(mess_out, 2, 0x23, 0x23, 0x23, code, size);
  	serial_write(fd, mess_out);
}
////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
void crea_codice_casa(char* code1, char* code2) {
	printf("Inserisci un codice di 5 cifre che servirà per accedere ogni volta alla tua casa\n");
	printf("$> ");
	fgets(code1, 7, stdin);
		
	while((strlen(code1) != 6) || (code1[5] != '\n')) {
		if(code1[6] != '\n') free_buf();
		printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
		printf("$> ");
		fgets(code1, 7, stdin); 
	}
		
	printf("Inserisci nuovamente il codice per conferma\n");
	printf("$> ");
	fgets(code2, 7, stdin);
		
	while((strlen(code2) != 6) || (code2[5] != '\n')) {
		if(code2[6] != '\n') free_buf();
		printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
		printf("$> ");
		fgets(code2, 7, stdin); 
	}
		
	while(strncmp(code1, code2, 5) != 0) {
		printf("\nCodici errati! Renserisci un codice di 5 cifre che servirà per accedere ogni volta alla tua casa\n");
		memset(code1, 0, 7);
		memset(code2, 1, 7);
		
		while((strlen(code1) != 6) || (code1[5] != '\n')) {
			if(code1[6] != '\n') free_buf();
			printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
			printf("$> ");
			fgets(code1, 7, stdin); 
		}
		printf("Inserisci nuovamente il codice per conferma\n");
		while((strlen(code2) != 6) || (code2[5] != '\n')) {
			if(code2[6] != '\n') free_buf();
			printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
			printf("$> ");
			fgets(code2, 7, stdin); 
		}
	}
  	return;
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void cambia_codice_casa(int fd, char* mess_out, char* code1, char* code2) {

	printf("Inserisci il vecchio codice\n");
	printf("$> ");
	fgets(code1, 7, stdin);
				
	while((strlen(code1) != 6) || (code1[5] != '\n')) {
		if(code1[6] != '\n') free_buf();
		printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
		printf("$> ");
		fgets(code1, 7, stdin); 
	}
				
	printf("Inserisci il nuovo codice\n");
	printf("$> ");
	fgets(code2, 7, stdin);
		
	while((strlen(code2) != 6) || (code2[5] != '\n')) {
		if(code2[6] != '\n') free_buf();
		printf("Numero di cifre errato! Inserisci un codice di 5 cifre\n");
		printf("$> ");
		fgets(code2, 7, stdin); 
	}
				
	uint8_t cd[10];
	for(int i = 0; i < 5; i++) {
		cd[i] = code1[i];
	}
	int j = 5;
	for(int i = 0; i < 5 ; i++) {
		cd[j] = code2[i];
		j++;			
	}
				
	int size = strlen(cd);
	UART_createBuf(mess_out, 3, 0x2a, 0x23, 0x23, cd, size);
  	serial_write(fd, mess_out);
}
/////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////
void crea_device(int fd, char* mess_out, char* dev_name, char* dev_type) {

	memset(dev_name, '\n', 19);
	printf("Inserisci il nome che vuoi assegnare al dispositivo.\n Ricorda che deve avere massimo 17 caratteri inclusi gli spazi!\n");
	printf("$> ");
	fgets(dev_name, 19, stdin);
							
	while(strlen(dev_name) > 17) {
		if(dev_name[17] != '\n') free_buf();
		printf("Numero di lettere errato! Inserisci un nome di massimo 17 caratteri\n");
		printf("$> ");
		fgets(dev_name, 19, stdin); 
	}
	printf("Seleziona il tipo di dispositivo che stai inserendo tra:\n 1 - luce regolabile\n 2 - luce normale o altro\n 3 - sensore di movimento\n 4 - sensore di temperatura e umidità\n\n");
	printf("$> ");
	fgets(dev_type, 3, stdin);
			
	while(((dev_type[0]-'0' != 1) && (dev_type[0]-'0' != 2) && (dev_type[0]-'0' != 3) && (dev_type[0]-'0' != 4)) || (strlen(dev_type) != 2) || (dev_type[1] != '\n')) {
		if(dev_type[1] != '\n') free_buf();
		printf("Numero errato! Riscrivi il tipo di dispositivo che stai inserendo tra:\n 1 - luce regolabile\n 2 - luce normale o altro\n 3 - sensore di movimento\n 4 - sensore di temperatura e umidità\n\n");
		printf("$> ");
		fgets(dev_type, 3, stdin);
	}
				
	int size = strlen(dev_name) - 1;
				
	UART_createBuf(mess_out, 5, 0x40, 0x23, (uint8_t)dev_type[0], dev_name, size);
  	serial_write(fd, mess_out);
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void elimina_device(int fd, char* mess_out, char* dev_type) {
	char c[1];
	char d[1] = {0x23};
	printf("\nScrivi la lettera corrispondente al device da eliminare (carattere prima del nome)\n");
	printf("$> ");
		
	fgets(dev_type, 3, stdin);
    	while((strlen(dev_type) != 2) || (dev_type[1] != '\n')) {
		if(dev_type[2] != '\n') free_buf();
		printf("Numero di lettere errato! Reinserisci la lettera corrispondente al device\n");
		printf("$> ");
		fgets(dev_type, 3, stdin); 
	}	
								
	c[0] = (char)toupper(dev_type[0]);
				
	int size = strlen(d);
	UART_createBuf(mess_out, 3, 0x5c, (uint8_t)c[0], 0x23, d, size);
  	serial_write(fd, mess_out);
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void chiedi_temperatura(int fd, char* mess_out) {
	char d[1] = {0x23};
	
	printf("Chiederò subito la temperatura e l'umidità alla tua casa!\n");
	int size = strlen(d);
	UART_createBuf(mess_out, 4, 0x3f, 0x4C, 0x23, d, size);
	serial_write(fd, mess_out);
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void stampa_temperatura(char* mess_in) {
  				
  	int thi = (int)mess_in[5];
  	int thd = (int)mess_in[6];
  	int humi = (int)mess_in[3];
  	int humd = (int)mess_in[4];
  				
  	printf("La temperatura è %d,%d°\n", thi, thd);
  	printf("L'umidità è del %d,%d%%\n", humi, humd);
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void accendi_device(int fd, char* mess_out, char* dev_type) {
	char c[1];
	char d[1] = {0x23};
	printf("\n\nScrivi la lettera corrispondente al device da accendere (carattere prima del nome)\n");
	printf("$> ");
				
	fgets(dev_type, 3, stdin);
    	while((strlen(dev_type) != 2) || (dev_type[1] != '\n')) {
		if(dev_type[2] != '\n') free_buf();
		printf("Numero di lettere errato! Reinserisci la lettera corrispondente al device\n");
		printf("$> ");
		fgets(dev_type, 3, stdin); 
	}	
				
	c[0] = (char)toupper(dev_type[0]);
				
	int size = strlen(d); 
	UART_createBuf(mess_out, 4, 0x2b, (uint8_t)c[0], 0x23, d, size);
  	serial_write(fd, mess_out);

}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void spegni_device(int fd, char* mess_out, char* dev_type) {
	char c[1];
	char d[1] = {0x23};
	printf("\n\nScrivi la lettera corrispondente al device da spegnere (carattere prima del nome)\n");
	printf("$> ");
				
	fgets(dev_type, 3, stdin);
    	while((strlen(dev_type) != 2) || (dev_type[1] != '\n')) {
		if(dev_type[2] != '\n') free_buf();
		printf("Numero di lettere errato! Reinserisci la lettera corrispondente al device\n");
		printf("$> ");
		fgets(dev_type, 3, stdin); 
	}	
				
	c[0] = (char)toupper(dev_type[0]);
			
	int size = strlen(d);
	UART_createBuf(mess_out, 4, 0x2d, (uint8_t)c[0], 0x23, d, size);
  	serial_write(fd, mess_out);
  	
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void regola_luce(int fd, char* mess_out, char* dev_type, char* input, device* dev, house* myhouse) {
	char c[1];
	char d[1] = {0x23};
	printf("Digita 1 se vuoi aumentare l'intensità di una luce; \n Digita 0 se vuoi diminuire l'intensità di una luce \n");
  	printf("$> ");
  				
  	fgets(input, 3, stdin);
	while((input[0]-'0' != 0 && input[0]-'0' != 1) || (strlen(input) != 2) || (input[1] != '\n')){
		if(input[1] != '\n') free_buf();
		printf("Riprova a digitare il numero corretto, è semplice!\n");
		printf("$> ");
		fgets(input, 3, stdin);
	}
				
	//stampo la struttura
	dev = myhouse->head;
	for(int i = 0; i < 9; i++) {
		print_devices(dev);
		dev = dev->next;
	}
	print_devices(dev); 
				
	printf("\nScrivi la lettera corrispondente al device da modificare (carattere prima del nome)\n");
	printf("$> ");
				
	fgets(dev_type, 3, stdin);
    	while((strlen(dev_type) != 2) || (dev_type[1] != '\n')) {
		if(dev_type[2] != '\n') free_buf();
		printf("Numero di lettere errato! Reinserisci la lettera corrispondente al device\n");
		printf("$> ");
		fgets(dev_type, 3, stdin); 
	}
					
	c[0] = (char)toupper(dev_type[0]);
  				
	if(input[0]-'0' == 0) { 
		int size = strlen(d);
		UART_createBuf(mess_out, 4, 0x3c, (uint8_t)c[0], 0x23, d, size);
  		serial_write(fd, mess_out);
	}
				
	else if(input[0]-'0' == 1) { 
		int size = strlen(d);
		UART_createBuf(mess_out, 4, 0x3e, (uint8_t)c[0], 0x23, d, size);
  		serial_write(fd, mess_out);
	}
}
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////
void info_device(int fd, char* mess_out, char* dev_type) {
	char c[1];
	char d[1] = {0x23};
	printf("Scrivi la lettera corrispondente al device (carattere prima del nome)\n");
	printf("$> ");
				
	fgets(dev_type, 3, stdin);
    	while((strlen(dev_type) != 2) || (dev_type[1] != '\n')) {
		if(dev_type[2] != '\n') free_buf();
		printf("Numero di cifre errato! Reinserisci la lettera corrispondente al device\n");
		printf("$> ");
		fgets(dev_type, 3, stdin); 
	}	
				
	c[0] = (char)toupper(dev_type[0]);
				
	int size = strlen(d);
	UART_createBuf(mess_out, 4, 0x3f, (uint8_t)c[0], 0x23, d, size);
  	serial_write(fd, mess_out);
}
/////////////////////////////////////////////////////////////////////////////////////




