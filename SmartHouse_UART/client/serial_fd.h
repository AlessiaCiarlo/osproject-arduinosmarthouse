#include <sys/types.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>


//apro il file descriptor sul quale è mappata la porta connessa all'arduino
int serial_open(const char* name);  

void flush_read(int fd);

//setto gli attributi 
int serial_set_interface_attribs(int fd, int speed);

void serial_read(int fd, char* mess);

int serial_write(int fd, char* mess);

void UART_createBuf(char* buff, int type, uint8_t action, uint8_t device, uint8_t device_type, char* data, int size);

int num_bit(char* mess);
