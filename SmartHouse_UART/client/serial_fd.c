#include "serial_fd.h"


int serial_open(const char* name) {					
  int fd = open(name, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0) {
    printf ("error %d opening serial, fd %d\n", errno, fd);
  }
  return fd;
}

int serial_set_interface_attribs(int fd, int speed){
  struct termios tty;
  memset(&tty, 0, sizeof tty);
  if(tcgetattr(fd, &tty) != 0){ 
    printf("Error %d from tcgetattr\n", errno);
    return -1;
  }

  speed_t baudrate = B9600;
 
  cfsetospeed(&tty, baudrate);
  cfsetispeed(&tty, baudrate);
  cfmakeraw(&tty);
  
  tty.c_oflag = 0;
  tty.c_cc[VMIN] = 0;
  tty.c_cc[VTIME] = 20; 

  //set the data format: 8 bit with 1 stop bit, no parity (raw mode)
  tty.c_cflag &= ~PARENB;
  tty.c_cflag &= ~CSTOPB;
  tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
  tty.c_cflag &= ~CRTSCTS;
  
  tty.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
  tty.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

  tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
  tty.c_oflag &= ~OPOST; // make raw

  //now let's set the attributes
  if (tcsetattr(fd, TCSANOW, &tty) != 0){
    printf("Error %d from tcsetattr \n", errno);
    return -1;
  }
  return 0;
}



void flush_read(int fd){
  tcflush(fd,TCIFLUSH);
}


void serial_read(int fd, char* mess){
  	flush_read(fd);
	char m[1];
 	int i = 0, j = 0;
  	do {
    		int cread = read(fd, m, 1); //leggo un char alla volta
    		if (cread == -1){
      			printf("Error while reading data from the serial port");
      			return;
    		}
    		if (cread == 0 && j == 1){
    			return;
    		}
    		if (cread == 0){
      			usleep(1000); //do più tempo per scrivere (attendo)
      			j++;
      			continue;
    		}
   			
    		mess[i] = m[0];
    		i++;
  	} while(m[0] != '\n');
  	mess[i] = 0; 			//termino la stringa
  	return;
}



//il client genera i messaggi con la funzione UART_createBuf
//ma scrive poi questo messaggio su file 
int serial_write(int fd, char* mess){
  	int mess_len = strlen(mess);		//lunghezza
  	int written = 0;
  	while(written < mess_len){
    		written += write(fd, mess, mess_len - written); 
    		if (written == -1){
     			printf("Errore durante la scrittura su fd");
      			exit(EXIT_FAILURE);
    		}
  	}
  	return written;
}





//creo il messaggio da inviare usando poi serial_write 
void UART_createBuf(char* buff, int type, uint8_t action, uint8_t device, uint8_t device_type, char* data, int size) {
	
	if (type == 2) { 				// ENTRA 
		buff[0] = 56;
		buff[1] = 0x1e; 			//^^ ENTRA
		buff[2] = data[0];
		buff[3] = data[1];
		buff[4] = data[2];
		buff[5] = data[3];
		buff[6] = data[4];
		buff[7] = '\n'; 			//line feed = \n terminatore
	}
	else if (type == 3) { 				// MODIFICA 
		if (action == 0x2a) { 			//azione = cambia codice 
			buff[0] = 104;
			buff[1] = 0x3d; 		//= MODIFICA
			buff[2] = action; 		//cambia codice
			buff[3] = data[0]; 		//vecchio codice
			buff[4] = data[1];
			buff[5] = data[2];
			buff[6] = data[3];
			buff[7] = data[4]; 
			
			buff[8] = data[5]; 		//nuovo codice
			buff[9] = data[6];
			buff[10] = data[7];
			buff[11] = data[8];
			buff[12] = data[9];
			buff[13] = '\n'; 		//line feed = \n terminatore
		}
		else if(action == 0x5c) {		 		 
			buff[0] = 32;
			buff[1] = 0x3d; 		//= MODIFICA
			buff[2] = action;		//azione = elimina device
			buff[3] = device;
			buff[4] = '\n'; 		//line feed = \n terminatore
		}
		else if(action == 0x2f) {
			buff[0] = 64;
			buff[1] = 0x3d; 		//= MODIFICA
			buff[2] = action;		//azione = elimina casa
			buff[3] = data[0]; 		//codice casa
			buff[4] = data[1];
			buff[5] = data[2];
			buff[6] = data[3];
			buff[7] = data[4]; 
			buff[8] = '\n'; 		//line feed = \n terminatore
		}
	}
	else if (type == 4) { 				// AZIONE 
		buff[0] = 32;
		buff[1] = 0x5e; 			//^ AZIONE
		buff[2] = action;
		buff[3] = device;
		buff[4] = '\n'; 			//line feed = \n terminatore
	}
	else if (type == 5) { 				//CREA
		if (action == 0x3F) { 			//codice casa 
			buff[0] = 64;
			buff[1] = 0x26; 		//& CREA
			buff[2] = action; 		//crea codice casa (prima volta)
			buff[3] = data[0];
			buff[4] = data[1];
			buff[5] = data[2];
			buff[6] = data[3];
			buff[7] = data[4];
			buff[8] = '\n'; 		//line feed = \n terminatore
		}
		else if (action == 0x40) { 		//crea device 
			buff[0] = 32 + (size * 8);    
			buff[1] = 0x26; 		//& CREA
			buff[2] = action; 		//crea device
			buff[3] = device_type; 		//tipo nuovo device 
			for (int i = 0; i < size; i++) { //scrivo il nome del dev nel messaggio
				buff[4 + i] = data[i];
			}
			buff[5 + size - 1] = '\n'; 	//line feed = \n terminatore
		}
	}
	else if (type == 6) { 
		buff[0] = 16;
		buff[1] = 0x58; 			//X ESCI
		buff[2] = '\n';				//line feed = \n terminatore
	}
	

	//printf("%s\n", buff);
	return;
}


int num_bit(char* mess) {
	int b = 0;
	while(*mess){
    		b += 8;
    		++mess;
  	}
  	return b;
}



