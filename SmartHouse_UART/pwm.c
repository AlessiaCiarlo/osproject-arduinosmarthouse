#include "dev.h"


void pwm_init(void) {
	//pin 8,11,12,13 --> bit 5,6,7 della porta B e bit 5 della porta H
	const uint8_t mask = 0b11100000; 
    	DDRB |= mask;  //pin configurati come output  
    	DDRH |= (1<<PH5);  	
    	
    	// uso timer 1 e 2 perchè connessi ai pin che mi servono 
  	TCCR1A = (1<<WGM10)|(1<<COM1C0)|(1<<COM1C1)|(1<<COM1B0)|(1<<COM1B1)|(1<<COM1A0)|(1<<COM1A1);
  	TCCR1B = (1<<WGM12)|(1<<CS10);
  	TCCR4A = (1<<WGM40)|(1<<COM4C0)|(1<<COM4C1)|(1<<COM4B0)|(1<<COM4B1)|(1<<COM4A0)|(1<<COM4A1);
 	TCCR4B = (1<<WGM42)|(1<<CS40);
 	
	// bit output compare per i timer
	OCR4CH = 255;	//PIN 8
  	OCR4CL = 255;	
	OCR1AH = 255;	//PIN 11
  	OCR1AL = 255;	
  	OCR1BH = 255;	//PIN 12
  	OCR1BL = 255;	
  	OCR1CH = 255;	//PIN 13
  	OCR1CL = 255;	
}


//segno sul server le intensità dei 4 led (inizialmete 0 e ogni volta che incrementano/decrementano aggiungo/tolgo 1)
//quando il client chiede di alzare o abbassare l'intensità, il server aumenta il suo valore di intensità salvato 
//e chiama la funzione pwm_change passandogli il nuovo valore (se si è arrivati al massimo invia errore) 

void pwm_change(uint8_t led, uint8_t intensity) {
	switch(led) {
		case 0x41: 	//pin 8 
			if (intensity == 0) OCR4CL = 255;
			else if (intensity == 1) OCR4CL = 190;
			else if (intensity == 2) OCR4CL = 120;
			else if (intensity == 3) OCR4CL = 80;
			else if (intensity == 4) OCR4CL = 30;
			else if (intensity == 5) OCR4CL = 0;
			break;
		case 0x42:	//pin 11
			if (intensity == 0) OCR1AL = 255;
			else if (intensity == 1) OCR1AL = 190;
			else if (intensity == 2) OCR1AL = 120;
			else if (intensity == 3) OCR1AL = 80;
			else if (intensity == 4) OCR1AL = 30;
			else if (intensity == 5) OCR1AL = 0;
			break;
		case 0x43:	//pin 12
			if (intensity == 0) OCR1BL = 255;
			else if (intensity == 1) OCR1BL = 190;
			else if (intensity == 2) OCR1BL = 120;
			else if (intensity == 3) OCR1BL = 80;
			else if (intensity == 4) OCR1BL = 30;
			else if (intensity == 5) OCR1BL = 0;
			break;
		case 0x44:	//pin 13
			if (intensity == 0) OCR1CL = 255;
			else if (intensity == 1) OCR1CL = 190;
			else if (intensity == 2) OCR1CL = 120;
			else if (intensity == 3) OCR1CL = 80;
			else if (intensity == 4) OCR1CL = 30;
			else if (intensity == 5) OCR1CL = 0;
			break;
	}
	return;
}




void pwm_all_off(void) {
	OCR4CH = 255;	//PIN 8
  	OCR4CL = 255;	
	OCR1AH = 255;	//PIN 11
  	OCR1AL = 255;	
  	OCR1BH = 255;	//PIN 12
  	OCR1BL = 255;	
  	OCR1CH = 255;	//PIN 13
  	OCR1CL = 255;	
}




void pwm_pulse(uint8_t led) {
	uint8_t intensity = 0;
	switch(led) {
		case 0x41:
			while(1){
    				OCR4CL = intensity; 
    				_delay_ms(100); // from delay.h, wait 1 sec
    				intensity += 1;
  			}
			break;
		case 0x42:
			while(1){
    				OCR1AL = intensity; 
    				_delay_ms(100); // from delay.h, wait 1 sec
    				intensity += 255;
  			}
			break;
		case 0x43:
			while(1){
    				OCR1BL = intensity; 
    				_delay_ms(100); // from delay.h, wait 1 sec
    				intensity += 1;
  			}
			break;
		case 0x44:
			while(1){
    				OCR1CL = intensity; 
    				_delay_ms(100); // from delay.h, wait 1 sec
    				intensity += 1;
  			}
			break;
	}
}




