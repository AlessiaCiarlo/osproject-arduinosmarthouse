#include "dev.h"


/*
Sensore di temperatura mappato su pin 52 --> bit 1 della porta B (PB1)

riceverò 40 bt così composti:
	High humidity --> parte intera 
	Low humidity  --> parte decimale
	High temperature --> parte intera  
	Low temperature --> parte decimale
	Parity --> bit di parità per testare il corretto arrivo 
*/


uint8_t read(void){
	uint8_t byte = 0;
	uint8_t time = 0;
	for (int i = 0; i < 8; i++) {
		//attendo finchè il segnale è alto (circa 30-50 us)
    		while((PINB & (1<<PB1)) == 0);	
    		_delay_us(30);
    		
    		//dopo il 28-esimo us di attesa controllo se il segnale è alto o basso e, a seconda di ciò, leggo il bit 1 o il bit 0 e lo inserisco in byte
    		if((PINB & (1<<PB1)) == 0) byte = (byte<<1)|1;
    		else byte = (byte<<1);
    		
    		time = 0;
    		while((PINB & (1<<PB1))) { 	//se è ancora basso aspetto 
    			_delay_us(1);
			time++;
			if(time > 70) break;	
    		}
  	}
  	return byte;
}	



void th_hum_read(uint8_t dev_byte){
	uint8_t data[5]; 
	uint8_t thi, thd, humi, humd;
	uint8_t time = 0;
	
	DDRB |= (1<<PB1); //output
	//invio request --> bus pull down per più di 18 ms
	PORTB &= ~(1<<PB1);
	_delay_ms(20);
	//bus pull up per 20-40 microsecondi
	PORTB |= (1<<PB1);
	_delay_us(25);
	
	DDRB &= ~(1<<PB1); //input
	//ricevo response --> aspetto 80 microsecondi pull down 
	while(PINB & (1<<PB1)){		
		_delay_us(1);
		if(time > 80) break;
		time++;
	}
	//azzero il contatore e aspetto 80 microsecondi pull up 
	time = 0;
	while((PINB & (1<<PB1)) == 0){		
		_delay_us(1);
		if(time > 80) break;
		time++;
	}
	//riazzero il contatore e aspetto 80 microsecondi pull down 
	time = 0;
	while(PINB & (1<<PB1)){		
		_delay_us(1);
		if(time > 80) break;
		time++;
	}
	
	//inizio a ricevere i dati che saranno composti da 50us di segnale basso
	//e 27 o 70 us di canale alto a seconda del bit 
	//(27 = Low(0) e 70 = High(1))
	data[0] = read();
	data[1] = read();
	data[2] = read();
	data[3] = read();
	data[4] = read();
	
	uint8_t parity = (data[0] + data[1] + data[2] + data[3] + 3);   
  	if(parity != data[4]) {
  		uint8_t error_mess[10];
  		uint8_t nada[1] = {0x23};
  		UART_createBuf(error_mess, 6, 0x61, dev_byte, 0x23, nada, 1);
  		UART_putString(error_mess);
  		return;
	}
	
	//inversione
  	humi = 0xFF - data[0];
  	humd = 0xFF - data[1];
  	thi = 0xFF - data[2];
  	thd = 0xFF - data[3];
    	
  	uint8_t m[4] = {humi, humd, thi, thd}; 
  	uint8_t mess[10];
  	UART_createBuf(mess, 1, 0x23, dev_byte, 0x23, m, 4);
  	UART_putString(mess);
  	return;
}










